angular.module('mainApp', [
  'ui.router',
  'dataGrid',
  'pagination',
  'angularFileUpload',
  'ngAnimate',
  'toastr',
  'froala',
  'ngQuickDate'])

  .config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('index', {
        url: '/',
        templateUrl: 'app/views/index.html',
        controller: 'IndexController'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'app/views/login.html',
        controller: 'LoginController'
      })
      .state('posts', {
        url: '/posts',
        templateUrl: 'app/views/posts.html',
        controller: 'PostsController',
        controllerAs: 'postsCtrl'
      })
      .state('post_category', {
        url: '/post_category',
        templateUrl: 'app/views/post-category.html',
        controller: 'PostCategoryController',
        controllerAs: '$ctrl'
      })
      .state('post_category_item', {
        url: '/post_category_item/:id',
        templateUrl: 'app/views/post-category-edit.html',
        controller: 'PostCategoryFormController',
        controllerAs: '$ctrl'
      })
      .state('editor', {
        url: '/editor',
        templateUrl: 'app/views/editor.html',
        controller: 'EditorController',
        controllerAs: 'editorCtrl'
      })
      .state('post', {
        url: '/post/:id',
        templateUrl: 'app/views/post-edit.html',
        controller: 'PostController',
        controllerAs: 'postCtrl'
      });

    $urlRouterProvider.otherwise('/login');
  })

  .run(function($rootScope, $state, $log, AuthService, AUTH_EVENTS) {
    $rootScope.$on('$stateChangeStart', function(event, next, nextParams, fromState) {
      if (!AuthService.isAuthenticated()) {
        if (next.name !== 'login' && next.name !== 'register') {
          event.preventDefault();
          $state.go('login');
        }
      }
    });

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
        if (!angular.isString(error)) {
          error = JSON.stringify(error);
        }
        $log.error('$stateChangeError: ' + error);
      }
    );

  });

require('./load');