//ngQuickDate https://github.com/adamalbrecht/ngQuickDate
require('../bower_components/ng-quick-date/dist/ng-quick-date.min');

// Controllers
require('./controllers/AppController');
require('./controllers/AppController');
require('./controllers/AppController');
require('./controllers/EditorController');
require('./controllers/IndexController');
require('./controllers/LoginController');
require('./controllers/PostController');
require('./controllers/PostsController');
require('./controllers/PostCategoryController');
require('./controllers/PostCategoryFormController');

// Views
require('./views/editor.html');
require('./views/index.html');
require('./views/login.html');
require('./views/menu.html');
require('./views/post-edit.html');
require('./views/post-form.html');
require('./views/posts.html');
require('./views/post-category.html');
require('./views/post-category-edit.html');
require('./views/post-category-form.html');

// Services
require('./services/services');
require('./services/rest');

// Constants
require('./constants/constants');
