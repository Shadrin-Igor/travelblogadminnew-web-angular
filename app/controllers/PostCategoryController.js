(function() {

  'use strict';

  angular
    .module('mainApp')

    /**
     * IndexController
     * Description: Sets up a controller
     */
    .controller('PostCategoryController', PostCategoryController);
  function PostCategoryController($scope, $log, $http, RestService, API_ENDPOINT, $state, toastr, FileUploader) {

    $scope.list = [];
    $scope.gridActions = {};
    $scope.gridOptions = {
      data: [],
      sort: {},
      urlSync: true
    };

    loadData();

    $scope.delete = function(id) {
      if (confirm('Вы подтверждаете удаление?')) {
        RestService.delete('post_category/' + id)
          .then(function(response) {
            if (response.data.status === 'deleted') {
              toastr.error('Запись успешно удаленна');
              loadData();
            }
            else {
              toastr.info('Произошла ошибка удаления');
            }
          })
      }
    };

    function loadData() {
      RestService.get('post_category', {})
        .then(function(response) {
          $('table.table').show(400);
          $scope.list = response.data.data;
          $scope.gridOptions.data = response.data.data;
          console.log('get post category', response);
        })
    };
  };
})();