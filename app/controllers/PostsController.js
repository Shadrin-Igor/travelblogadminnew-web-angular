(function() {

  'use strict';

  angular
    .module('mainApp')

    /**
     * IndexController
     * Description: Sets up a controller
     */
    .controller('PostsController', PostsController);
  function PostsController($scope, $log, $http, RestService, API_PUBLIC, API_ENDPOINT, $state, toastr, FileUploader) {

    $scope.$watch(function() {
      return $state.params.cat
    }, function(newVal, oldVal) {
      $log.log('$watch $state', newVal);
    });

    $scope.listProducts = [];
    $scope.cat = $state.params.cat;
    $scope.name = 'Посты';
    $scope.find = {};
    $scope.gridActions = {};
    $scope.publicUrl = API_PUBLIC.url;

    $scope.gridOptions = {
      data: [],
      sort: {},
      urlSync: true
    };

    loadData();

    $scope.uploader = new FileUploader({
      url: API_ENDPOINT.url + '/import/' + $scope.cat,
      headers: {Authorization: $http.defaults.headers.common.Authorization},
      autoUpload: true
    });

    $scope.uploader.onSuccessItem = function(res, responce) {
      console.log('Загрузил');
      if (responce.status === 'ok') {
        $('#before-load').fadeOut(200, function() {
          toastr.info('Файл успешно загружен');
          $scope.listProducts = responce.items;
        });
      } else {
        $('#before-load').fadeOut(200, function() {
          toastr.error(responce.error, 'Ошибка загрузки файла');
          $scope.listProducts = [];
        });
      }

    };

    $scope.uploader.onBeforeUploadItem = function() {
      $('#before-load').fadeIn(200);
    };

    $scope.uploader.onErrorItem = function(fileItem, response, status, headers) {
      console.info('onErrorItem', fileItem, response, status, headers);
      $('#before-load').fadeOut(200, function() {
        toastr.info('Произошла ошибка закачки Файла');
      });
    };

    $scope.delete = function(id) {
      if (confirm('Вы подтверждаете удаление?')) {
        RestService.delete('post/' + id)
          .then(function(response) {

            if (response.data.status === 'deleted') {
              toastr.error('Продукция успешно удаленна');
              loadData();
            }
            else {
              toastr.info('Произошла ошибка удаления');
            }
          })
      }
    }

    function loadData() {
      RestService.get('post', {})
        .then(function(response) {
          $('table.table').show(400);
          $scope.listProducts = response.data.data;
          $scope.gridOptions.data = response.data.data;
          console.log('get products', response);
        })
    };
  };
})();