(function () {

    'use strict';

    angular
        .module('mainApp')

        /**
         * IndexController
         * Description: Sets up a controller
         */
        .controller('EditorController', EditorController);
    function EditorController($scope, $log, $http, RestService, API_ENDPOINT, $state, toastr, FileUploader) {

        $scope.titleOptions2 = {
            imageUploadURL: '/api/editor-upload'
        };

        $scope.titleOptions = {
            placeholderText: 'Add a Title',
            charCounterCount: false,
            toolbarInline: true,
            events: {
                'froalaEditor.initialized': function() {
                    console.log('initialized');
                }
            }
        };

        $scope.initialize = function(initControls) {
            $scope.initControls = initControls;
            $scope.deleteAll = function() {
                initControls.getEditor()('html.set', '');
            };
        };

        $scope.myTitle = '<span style="font-family: Verdana,Geneva,sans-serif; font-size: 30px;">My Document\'s Title</span><span style="font-size: 18px;"></span></span>';
        $scope.sample2Text = '';
        $scope.sample3Text = '';

        $scope.imgModel = {src: 'image.jpg'};

        $scope.buttonModel = {innerHTML: 'Click Me'};

        $scope.inputModel = {placeholder: 'I am an input!'};
        $scope.inputOptions = {
            angularIgnoreAttrs: ['class', 'ng-model', 'id', 'froala']
        }

        $scope.initializeLink = function(linkInitControls) {
            $scope.linkInitControls = linkInitControls;
        };
        $scope.linkModel = {href: 'https://www.froala.com/wysiwyg-editor'}
        
    };
})();