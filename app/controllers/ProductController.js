(function () {

    'use strict';

    angular
        .module('mainApp')

        /**
         * IndexController
         * Description: Sets up a controller
         */
        .controller('ProductController', ProductController);
    function ProductController($scope, $log, $http, RestService, API_ENDPOINT, $state, toastr, FileUploader) {

        $scope.product = {};
        $scope.marks = [];
        $scope.collors = [];
        $scope.country = [];
        $scope.images = [];
        $scope.cat = $state.params.cat;
        $scope.id = $state.params.id;
        loadData();

        $scope.saveProduct = function (type) {

            if (!$scope.product.id) {
                RestService.post('product/' + $scope.cat + '/' + $scope.id, {data: $scope.product, type: $state.params.cat})
                    .then(function (response) {
                        if (!response.data.error) {
                            toastr.success('Продукция успешно добавленна');
                            $scope.product.id = response.data.data;
                        }
                        else toastr.error(response.data.error);
                    }, function (response) {
                        console.log('error-response', response);
                    });
            }
            else {
                RestService.put('product/' + $scope.cat + '/' + $scope.id, {data: $scope.product, type: $state.params.cat})
                    .then(function (response) {
                        if (!response.data.error)toastr.success('Продукция успешно обновленна');
                        else toastr.error(response.data.error);
                    }, function (response) {
                        console.log('error-response', response);
                    });
            }
        }

        $scope.delete = function (id) {
            if (confirm("Вы подтверждаете удаление?")) {
                RestService.delete("product/" + $state.params.cat + "/" + id)
                    .then(function (response) {

                        if (response.data.status == 'deleted') {
                            toastr.error('Продукция успешно удаленна');
                            loadData();
                        }
                        else {
                            toastr.info('Произошла ошибка удаления');
                        }
                    })
            }
        }

        $scope.uploader = new FileUploader({
            url: API_ENDPOINT.url + '/upload/' + $scope.cat + '/' + $scope.id,
            headers: {Authorization: $http.defaults.headers.common.Authorization},
            removeAfterUpload: true,
            autoUpload: true
        });

        $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
            $scope.images.push({dir: response.file.dir, image: response.file.image});
        };
        $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };

        function loadData() {
            RestService.get("product/" + $state.params.cat + "/" + $state.params.id, {})
                .then(function (response) {
                    $scope.product = response.data.data;
                    console.log('get product', response);
                });

            RestService.get("gallery/" + $state.params.cat + "/" + $state.params.id, {})
                .then(function (response) {
                    $scope.images = response.data.data;
                    console.log('get gallery', response);
                });

            RestService.get("marks", {})
                .then(function (response) {
                    $scope.marks = response.data.data;
                    console.log('get marks', response);
                });

            RestService.get("collors", {})
                .then(function (response) {
                    $scope.collors = response.data.data;
                    console.log('get marks', response);
                });

            RestService.get("countries", {})
                .then(function (response) {
                    $scope.country = response.data.data;
                    console.log('get marks', response);
                });
        };
    };
})();