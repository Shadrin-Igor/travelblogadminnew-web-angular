(function() {

  'use strict';

  angular
    .module('mainApp')

    /**
     * IndexController
     * Description: Sets up a controller
     */
    .controller('PostController', PostController);
  function PostController($scope, $log, $http, RestService, API_ENDPOINT, API_PUBLIC, $state, toastr, FileUploader) {

    $('#edit').froalaEditor({
      tabSpaces: 4
    });

    $scope.post = {};
    $scope.images = [];
    $scope.categories = [];
    $scope.cat = $state.params.cat;
    $scope.id = $state.params.id;
    $scope.API_PUBLIC = API_PUBLIC;
    $scope.froalaOptions = {
      imageUploadURL: API_ENDPOINT.url + '/editor-upload'
    };
    loadData();

    $scope.saveProduct = function() {
      if (!$scope.post.id) {
        RestService.post('post/' + $scope.id, {data: $scope.post})
          .then(function(response) {
            if (!response.data.error) {
              toastr.success('Позиция успешно добавленна');
              $scope.post.id = response.data.data;
            }
            else {
              toastr.error(response.data.error.message);
            }
          })
          .catch(function(response) {
            console.log('error-response', response);
            toastr.error(response);
          });
      }
      else {
        RestService.put('post/' + $scope.id, {data: $scope.post})
          .then(function(response) {
            if (!response.data.error) {
              toastr.success('Позиция успешно обновленна');
              $scope.post = response.data.data.item;
              console.log('post', $scope.post, response.data.data);
            }
            else {
              toastr.error(response.data.error.message);
            }
          })
          .catch(function(error) {
            toastr.error(response.data.error.errors.message);
          });
      }
    };

    $scope.delete = function(id) {
      if (confirm('Вы подтверждаете удаление?')) {
        RestService.delete('post/' + id)
          .then(function(response) {

            if (response.data.status === 'deleted') {
              toastr.error('Продукция успешно удаленна');
              loadData();
            }
            else {
              toastr.info('Произошла ошибка удаления');
            }
          })
      }
    };

    $scope.uploader = new FileUploader({
      url: API_ENDPOINT.url + '/upload/post/' + $scope.id,
      headers: {Authorization: $http.defaults.headers.common.Authorization},
      removeAfterUpload: true,
      autoUpload: true
    });

    $scope.deleteImage = function(imageId) {
      if (confirm('Вы подтверждаете удаление картинки?')) {
        RestService.delete('gallery/' + imageId)
          .then(function(response) {

            if (response.data.status === 'deleted') {
              toastr.success('Картинка успешно удаленна');

              RestService.get('gallery/post/' + $state.params.id, {})
                .then(function(response) {
                  $scope.images = response.data.data;
                });
            }
            else {
              toastr.error('Произошла ошибка удаления');
            }
          })
      }
    };

    $scope.uploader.onSuccessItem = function(fileItem, response, status, headers) {
      console.info('onSuccessItem', fileItem, response, status, headers);
      $scope.images.push({dir: response.file.dir, image: response.file.image});
    };
    $scope.uploader.onErrorItem = function(fileItem, response, status, headers) {
      console.info('onErrorItem', fileItem, response, status, headers);
    };

    function loadData() {
      if ($state.params.id) {
        RestService.get('post/' + $state.params.id, {})
          .then(function(response) {
            $scope.post = response.data.data;

            RestService.get('post_category/', {})
              .then(function(responseCategory) {
                $scope.categories = responseCategory.data.data;
              });
          });

        RestService.get('gallery/post/' + $state.params.id, {})
          .then(function(response) {
            $scope.images = response.data.data;
          });
      }
    };
  };
})();