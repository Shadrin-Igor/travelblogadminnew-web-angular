(function () {

    'use strict';

    angular
        .module('mainApp')

        /**
         * IndexController
         * Description: Sets up a controller
         */
        .controller('PostCategoryFormController', PostCategoryFormController);
    function PostCategoryFormController($scope, $log, $http, RestService, API_ENDPOINT, API_PUBLIC, $state, toastr, FileUploader) {

        $('#edit').froalaEditor({
            tabSpaces: 4
        });

        $scope.category = {};
        $scope.images = [];
        $scope.cat = $state.params.cat;
        $scope.id = $state.params.id;
        $scope.API_PUBLIC = API_PUBLIC;
        $scope.froalaOptions = {
            imageUploadURL: API_ENDPOINT.url + '/editor-upload'
        };
        loadData();

        $scope.saveCategory = function () {

            console.log('saveCategory', $scope.category);
            if (!$scope.category.id) {
                RestService.post('post_category/', {data: $scope.category})
                    .then(function (response) {
                        if (!response.data.error) {
                            toastr.success('Позиция успешно добавленна');
                            $scope.category.id = response.data.data;
                        }
                        else toastr.error(response.data.error);
                    }, function (response) {
                        console.log('error-response', response);
                    });
            }
            else {
                RestService.put('post_category/' + $scope.id, {data: $scope.category})
                    .then(function (response) {
                        if (!response.data.error) toastr.success('Позиция успешно обновленна');
                        else toastr.error(response.data.error);
                    }, function (response) {
                        console.log('error-response', response);
                    });
            }
        };

        $scope.delete = function (id) {
            if (confirm("Вы подтверждаете удаление?")) {
                RestService.delete("post_category/" + id)
                    .then(function (response) {

                        if (response.data.status === 'deleted') {
                            toastr.error('Продукция успешно удаленна');
                            loadData();
                        }
                        else {
                            toastr.info('Произошла ошибка удаления');
                        }
                    })
            }
        };

        $scope.uploader = new FileUploader({
            url: API_ENDPOINT.url + '/upload/post_category/' + $scope.id,
            headers: {Authorization: $http.defaults.headers.common.Authorization},
            removeAfterUpload: true,
            autoUpload: true
        });

        $scope.deleteImage = function(imageId){
            if (confirm("Вы подтверждаете удаление картинки?")) {
                RestService.delete("gallery/" + imageId)
                    .then(function (response) {

                        if (response.data.status === 'deleted') {
                            toastr.success('Картинка успешно удаленна');

                            RestService.get("gallery/post_category/" + $state.params.id, {})
                                .then(function (response) {
                                    $scope.images = response.data.data;
                                    console.log('gallery', response.data.data);
                                });
                        }
                        else {
                            toastr.error('Произошла ошибка удаления');
                        }
                    })
            }
        };

        $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
            $scope.images.push({dir: response.file.dir, image: response.file.image});
        };
        $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };

        function loadData() {
            if ($state.params.id) {
                RestService.get("post_category/" + $state.params.id, {})
                    .then(function (response) {
                        $scope.category = response.data.data;
                        console.log('get product', response);
                    });

                RestService.get("gallery/post_category/" + $state.params.id, {})
                    .then(function (response) {
                        $scope.images = response.data.data;
                        console.log('gallery', response.data.data);
                    });
            }
        };
    };
})();