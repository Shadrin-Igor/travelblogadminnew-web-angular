(function() {
    'use strict';

    angular.module('mainApp')

        .controller('AppController', function ($scope, $state, $rootScope, $log, AuthService, AUTH_EVENTS) {

            $log.info('!', AuthService.isAuthenticated());
            $scope.userAuth = AuthService.isAuthenticated();

            $scope.toggle = function(id){
                $(id).toggle(200);
            }

            $scope.logout = function() {
                $rootScope.$broadcast('logout', {});
                AuthService.logout();
                $state.go('login');
            };

            $scope.$on("login", function (event) {
                $log.info('login');
                $scope.userAuth = true;
            });

            $scope.$on("logout", function (event) {
                $log.info('logout');
                $scope.userAuth = false;
            });
        });
})();