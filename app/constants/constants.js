angular.module('mainApp')

  .constant('AUTH_EVENTS', {
    notAuthenticated: 'auth-not-authenticated'
  })

  .constant('API_PUBLIC', {
    url: 'http://127.0.0.1:8030/'
  })

  .constant('API_ENDPOINT', {
    url: 'http://127.0.0.1:8030/api' //188.120.241.78
  });