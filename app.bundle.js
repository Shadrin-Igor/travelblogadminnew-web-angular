var mainApp =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
    'use strict';

    angular.module('mainApp').controller('AppController', function ($scope, $state, $rootScope, $log, AuthService, AUTH_EVENTS) {

        $log.info('!', AuthService.isAuthenticated());
        $scope.userAuth = AuthService.isAuthenticated();

        $scope.toggle = function (id) {
            $(id).toggle(200);
        };

        $scope.logout = function () {
            $rootScope.$broadcast('logout', {});
            AuthService.logout();
            $state.go('login');
        };

        $scope.$on("login", function (event) {
            $log.info('login');
            $scope.userAuth = true;
        });

        $scope.$on("logout", function (event) {
            $log.info('logout');
            $scope.userAuth = false;
        });
    });
})();

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


angular.module('mainApp', ['ui.router', 'dataGrid', 'pagination', 'angularFileUpload', 'ngAnimate', 'toastr', 'froala', 'ngQuickDate']).config(function ($stateProvider, $urlRouterProvider) {

  $stateProvider.state('index', {
    url: '/',
    templateUrl: 'app/views/index.html',
    controller: 'IndexController'
  }).state('login', {
    url: '/login',
    templateUrl: 'app/views/login.html',
    controller: 'LoginController'
  }).state('posts', {
    url: '/posts',
    templateUrl: 'app/views/posts.html',
    controller: 'PostsController',
    controllerAs: 'postsCtrl'
  }).state('post_category', {
    url: '/post_category',
    templateUrl: 'app/views/post-category.html',
    controller: 'PostCategoryController',
    controllerAs: '$ctrl'
  }).state('post_category_item', {
    url: '/post_category_item/:id',
    templateUrl: 'app/views/post-category-edit.html',
    controller: 'PostCategoryFormController',
    controllerAs: '$ctrl'
  }).state('editor', {
    url: '/editor',
    templateUrl: 'app/views/editor.html',
    controller: 'EditorController',
    controllerAs: 'editorCtrl'
  }).state('post', {
    url: '/post/:id',
    templateUrl: 'app/views/post-edit.html',
    controller: 'PostController',
    controllerAs: 'postCtrl'
  });

  $urlRouterProvider.otherwise('/login');
}).run(function ($rootScope, $state, $log, AuthService, AUTH_EVENTS) {
  $rootScope.$on('$stateChangeStart', function (event, next, nextParams, fromState) {
    if (!AuthService.isAuthenticated()) {
      if (next.name !== 'login' && next.name !== 'register') {
        event.preventDefault();
        $state.go('login');
      }
    }
  });

  $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
    if (!angular.isString(error)) {
      error = JSON.stringify(error);
    }
    $log.error('$stateChangeError: ' + error);
  });
});

__webpack_require__(2);

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


//ngQuickDate https://github.com/adamalbrecht/ngQuickDate
__webpack_require__(3);

// Controllers
__webpack_require__(0);
__webpack_require__(0);
__webpack_require__(0);
__webpack_require__(4);
__webpack_require__(5);
__webpack_require__(6);
__webpack_require__(7);
__webpack_require__(8);
__webpack_require__(9);
__webpack_require__(10);

// Views
__webpack_require__(11);
__webpack_require__(12);
__webpack_require__(13);
__webpack_require__(14);
__webpack_require__(15);
__webpack_require__(16);
__webpack_require__(17);
__webpack_require__(18);
__webpack_require__(19);
__webpack_require__(20);

// Services
__webpack_require__(21);
__webpack_require__(22);

// Constants
__webpack_require__(23);

/***/ }),
/* 3 */
/***/ (function(module, exports) {

(function(){var a;a=angular.module("ngQuickDate",[]),a.provider("ngQuickDateDefaults",function(){return{options:{dateFormat:"M/d/yyyy",timeFormat:"h:mm a",labelFormat:null,placeholder:"Click to Set Date",hoverText:null,buttonIconHtml:null,closeButtonHtml:"&times;",nextLinkHtml:"Next &rarr;",prevLinkHtml:"&larr; Prev",disableTimepicker:!1,disableClearButton:!1,defaultTime:null,dayAbbreviations:["Su","M","Tu","W","Th","F","Sa"],dateFilter:null,parseDateFunction:function(a){var b;return b=Date.parse(a),isNaN(b)?null:new Date(b)}},$get:function(){return this.options},set:function(a,b){var c,d,e;if("object"==typeof a){e=[];for(c in a)d=a[c],e.push(this.options[c]=d);return e}return this.options[a]=b}}}),a.directive("quickDatepicker",["ngQuickDateDefaults","$filter","$sce",function(a,b,c){return{restrict:"E",require:"?ngModel",scope:{dateFilter:"=?",onChange:"&",required:"@"},replace:!0,link:function(d,e,f,g){var h,i,j,k,l,m,n,o,p,q,r,s,t,u;return n=function(){var a;return r(),d.toggleCalendar(!1),d.weeks=[],d.inputDate=null,d.inputTime=null,d.invalid=!0,"string"==typeof f.initValue&&g.$setViewValue(f.initValue),d.defaultTime||(a=new Date(2013,0,1,12,0),d.datePlaceholder=b("date")(a,d.dateFormat),d.timePlaceholder=b("date")(a,d.timeFormat)),q(),p()},r=function(){var b,e;for(b in a)e=a[b],b.match(/[Hh]tml/)?d[b]=c.trustAsHtml(a[b]||""):!d[b]&&f[b]?d[b]=f[b]:d[b]||(d[b]=a[b]);return d.labelFormat||(d.labelFormat=d.dateFormat,d.disableTimepicker||(d.labelFormat+=" "+d.timeFormat)),f.iconClass&&f.iconClass.length?d.buttonIconHtml=c.trustAsHtml("<i ng-show='iconClass' class='"+f.iconClass+"'></i>"):void 0},i=!1,window.document.addEventListener("click",function(a){return d.calendarShown&&!i&&(d.toggleCalendar(!1),d.$apply()),i=!1}),angular.element(e[0])[0].addEventListener("click",function(a){return i=!0}),p=function(){var a;return a=g.$modelValue?o(g.$modelValue):null,t(),s(a),d.mainButtonStr=a?b("date")(a,d.labelFormat):d.placeholder,d.invalid=g.$invalid},s=function(a){return null!=a?(d.inputDate=b("date")(a,d.dateFormat),d.inputTime=b("date")(a,d.timeFormat)):(d.inputDate=null,d.inputTime=null)},q=function(a){var b;return null==a&&(a=null),b=null!=a?new Date(a):new Date,"Invalid Date"===b.toString()&&(b=new Date),b.setDate(1),d.calendarDate=new Date(b)},t=function(){var a,b,c,e,f,h,i,k,l,n,o,p,q,r;for(h=d.calendarDate.getDay(),e=m(d.calendarDate.getFullYear(),d.calendarDate.getMonth()),f=Math.ceil((h+e)/7),o=[],a=new Date(d.calendarDate),a.setDate(a.getDate()+-1*h),i=p=0,r=f-1;r>=0?r>=p:p>=r;i=r>=0?++p:--p)for(o.push([]),c=q=0;6>=q;c=++q)b=new Date(a),d.defaultTime&&(l=d.defaultTime.split(":"),b.setHours(l[0]||0),b.setMinutes(l[1]||0),b.setSeconds(l[2]||0)),k=g.$modelValue&&b&&j(b,g.$modelValue),n=j(b,new Date),o[i].push({date:b,selected:k,disabled:"function"==typeof d.dateFilter?!d.dateFilter(b):!1,other:b.getMonth()!==d.calendarDate.getMonth(),today:n}),a.setDate(a.getDate()+1);return d.weeks=o},g.$parsers.push(function(a){return d.required&&null==a?(g.$setValidity("required",!1),null):angular.isDate(a)?(g.$setValidity("required",!0),a):angular.isString(a)?(g.$setValidity("required",!0),d.parseDateFunction(a)):null}),g.$formatters.push(function(a){return angular.isDate(a)?a:angular.isString(a)?d.parseDateFunction(a):void 0}),h=function(a,c){return b("date")(a,c)},u=function(a){return"string"==typeof a?o(a):a},o=a.parseDateFunction,j=function(a,b,c){return null==c&&(c=!1),c?a-b===0:(a=u(a),b=u(b),a&&b&&a.getYear()===b.getYear()&&a.getMonth()===b.getMonth()&&a.getDate()===b.getDate())},k=function(a,b){return a&&b?parseInt(a.getTime()/6e4)===parseInt(b.getTime()/6e4):!1},m=function(a,b){return[31,a%4===0&&a%100!==0||a%400===0?29:28,31,30,31,30,31,31,30,31,30,31][b]},l=function(a,b){var c,d,e,f,g,h;return g=c=d=h=f=null,e=function(){var a;return a=+new Date-h,g=b>a&&a>0?setTimeout(e,b-a):null},function(){return d=this,c=arguments,h=+new Date,g||(g=setTimeout(e,b),f=a.apply(d,c),d=c=null),f}},g.$render=function(){return q(g.$viewValue),p()},g.$viewChangeListeners.unshift(function(){return q(g.$viewValue),p(),d.onChange?d.onChange():void 0}),d.$watch("calendarShown",function(a,b){var c;return a?(c=angular.element(e[0].querySelector(".quickdate-date-input"))[0],c.select()):void 0}),d.toggleCalendar=l(function(a){return isFinite(a)?d.calendarShown=a:d.calendarShown=!d.calendarShown},150),d.selectDate=function(a,b){var c;return null==b&&(b=!0),c=!g.$viewValue&&a||g.$viewValue&&!a||a&&g.$viewValue&&a.getTime()!==g.$viewValue.getTime(),"function"!=typeof d.dateFilter||d.dateFilter(a)?(g.$setViewValue(a),b&&d.toggleCalendar(!1),!0):!1},d.selectDateFromInput=function(a){var b,c,e,f;null==a&&(a=!1);try{if(c=o(d.inputDate),!c)throw"Invalid Date";if(!d.disableTimepicker&&d.inputTime&&d.inputTime.length&&c){if(f=d.disableTimepicker?"00:00:00":d.inputTime,e=o(""+d.inputDate+" "+f),!e)throw"Invalid Time";c=e}if(!k(g.$viewValue,c)&&!d.selectDate(c,!1))throw"Invalid Date";return a&&d.toggleCalendar(!1),d.inputDateErr=!1,d.inputTimeErr=!1}catch(h){if(b=h,"Invalid Date"===b)return d.inputDateErr=!0;if("Invalid Time"===b)return d.inputTimeErr=!0}},d.onDateInputTab=function(){return d.disableTimepicker&&d.toggleCalendar(!1),!0},d.onTimeInputTab=function(){return d.toggleCalendar(!1),!0},d.nextMonth=function(){return q(new Date(new Date(d.calendarDate).setMonth(d.calendarDate.getMonth()+1))),p()},d.prevMonth=function(){return q(new Date(new Date(d.calendarDate).setMonth(d.calendarDate.getMonth()-1))),p()},d.clear=function(){return d.selectDate(null,!0)},n()},template:"<div class='quickdate'>\n  <a href='' ng-focus='toggleCalendar()' class='quickdate-button' title='{{hoverText}}'><div ng-hide='iconClass' ng-bind-html='buttonIconHtml'></div>{{mainButtonStr}}</a>\n  <div class='quickdate-popup' ng-class='{open: calendarShown}'>\n    <a href='' tabindex='-1' class='quickdate-close' ng-click='toggleCalendar()'><div ng-bind-html='closeButtonHtml'></div></a>\n    <div class='quickdate-text-inputs'>\n      <div class='quickdate-input-wrapper'>\n        <label>Date</label>\n        <input class='quickdate-date-input' ng-class=\"{'ng-invalid': inputDateErr}\" name='inputDate' type='text' ng-model='inputDate' placeholder='{{ datePlaceholder }}' ng-enter=\"selectDateFromInput(true)\" ng-blur=\"selectDateFromInput(false)\" on-tab='onDateInputTab()' />\n      </div>\n      <div class='quickdate-input-wrapper' ng-hide='disableTimepicker'>\n        <label>Time</label>\n        <input class='quickdate-time-input' ng-class=\"{'ng-invalid': inputTimeErr}\" name='inputTime' type='text' ng-model='inputTime' placeholder='{{ timePlaceholder }}' ng-enter=\"selectDateFromInput(true)\" ng-blur=\"selectDateFromInput(false)\" on-tab='onTimeInputTab()'>\n      </div>\n    </div>\n    <div class='quickdate-calendar-header'>\n      <a href='' class='quickdate-prev-month quickdate-action-link' tabindex='-1' ng-click='prevMonth()'><div ng-bind-html='prevLinkHtml'></div></a>\n      <span class='quickdate-month'>{{calendarDate | date:'MMMM yyyy'}}</span>\n      <a href='' class='quickdate-next-month quickdate-action-link' ng-click='nextMonth()' tabindex='-1' ><div ng-bind-html='nextLinkHtml'></div></a>\n    </div>\n    <table class='quickdate-calendar'>\n      <thead>\n        <tr>\n          <th ng-repeat='day in dayAbbreviations'>{{day}}</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr ng-repeat='week in weeks'>\n          <td ng-mousedown='selectDate(day.date, true, true)' ng-click='$event.preventDefault()' ng-class='{\"other-month\": day.other, \"disabled-date\": day.disabled, \"selected\": day.selected, \"is-today\": day.today}' ng-repeat='day in week'>{{day.date | date:'d'}}</td>\n        </tr>\n      </tbody>\n    </table>\n    <div class='quickdate-popup-footer'>\n      <a href='' class='quickdate-clear' tabindex='-1' ng-hide='disableClearButton' ng-click='clear()'>Clear</a>\n    </div>\n  </div>\n</div>"}}]),a.directive("ngEnter",function(){return function(a,b,c){return b.bind("keydown keypress",function(b){return 13===b.which?(a.$apply(c.ngEnter),b.preventDefault()):void 0})}}),a.directive("onTab",function(){return{restrict:"A",link:function(a,b,c){return b.bind("keydown keypress",function(b){return 9!==b.which||b.shiftKey?void 0:a.$apply(c.onTab)})}}})}).call(this);

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {

    'use strict';

    angular.module('mainApp')

    /**
     * IndexController
     * Description: Sets up a controller
     */
    .controller('EditorController', EditorController);
    function EditorController($scope, $log, $http, RestService, API_ENDPOINT, $state, toastr, FileUploader) {

        $scope.titleOptions2 = {
            imageUploadURL: '/api/editor-upload'
        };

        $scope.titleOptions = {
            placeholderText: 'Add a Title',
            charCounterCount: false,
            toolbarInline: true,
            events: {
                'froalaEditor.initialized': function froalaEditorInitialized() {
                    console.log('initialized');
                }
            }
        };

        $scope.initialize = function (initControls) {
            $scope.initControls = initControls;
            $scope.deleteAll = function () {
                initControls.getEditor()('html.set', '');
            };
        };

        $scope.myTitle = '<span style="font-family: Verdana,Geneva,sans-serif; font-size: 30px;">My Document\'s Title</span><span style="font-size: 18px;"></span></span>';
        $scope.sample2Text = '';
        $scope.sample3Text = '';

        $scope.imgModel = { src: 'image.jpg' };

        $scope.buttonModel = { innerHTML: 'Click Me' };

        $scope.inputModel = { placeholder: 'I am an input!' };
        $scope.inputOptions = {
            angularIgnoreAttrs: ['class', 'ng-model', 'id', 'froala']
        };

        $scope.initializeLink = function (linkInitControls) {
            $scope.linkInitControls = linkInitControls;
        };
        $scope.linkModel = { href: 'https://www.froala.com/wysiwyg-editor' };
    };
})();

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {

    'use strict';

    angular.module('mainApp')

    /**
     * IndexController
     * Description: Sets up a controller
     */
    .controller('IndexController', IndexController);
    function IndexController($scope, $rootScope, $http, $window, AuthService, API_ENDPOINT, $state) {
        $scope.destroySession = function () {
            AuthService.logout();
        };

        $scope.getInfo = function () {

            $http.get(API_ENDPOINT.url + '/post').then(function (result) {
                $scope.memberinfo = result.data.msg;
            });
        };
    };
})();

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {

    'use strict';

    angular.module('mainApp')

    /**
     * AppController
     * Description: Sets up a controller
     */
    .controller('LoginController', LoginController);
    function LoginController($scope, AuthService, $rootScope, $state) {
        $scope.user = {
            name: '',
            password: ''
        };

        $scope.login = function () {
            AuthService.login($scope.user).then(function (msg) {
                $rootScope.$broadcast('login', { user: $scope.user });
                $state.go('index');
            }, function (errMsg) {
                alert('Login failed!' + errMsg);
            });
        };
    };
})();

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {

  'use strict';

  angular.module('mainApp')

  /**
   * IndexController
   * Description: Sets up a controller
   */
  .controller('PostController', PostController);
  function PostController($scope, $log, $http, RestService, API_ENDPOINT, API_PUBLIC, $state, toastr, FileUploader) {

    $('#edit').froalaEditor({
      tabSpaces: 4
    });

    $scope.post = {};
    $scope.images = [];
    $scope.categories = [];
    $scope.cat = $state.params.cat;
    $scope.id = $state.params.id;
    $scope.API_PUBLIC = API_PUBLIC;
    $scope.froalaOptions = {
      imageUploadURL: API_ENDPOINT.url + '/editor-upload'
    };
    loadData();

    $scope.saveProduct = function () {
      if (!$scope.post.id) {
        RestService.post('post/' + $scope.id, { data: $scope.post }).then(function (response) {
          if (!response.data.error) {
            toastr.success('Позиция успешно добавленна');
            $scope.post.id = response.data.data;
          } else {
            toastr.error(response.data.error.message);
          }
        }).catch(function (response) {
          console.log('error-response', response);
          toastr.error(response);
        });
      } else {
        RestService.put('post/' + $scope.id, { data: $scope.post }).then(function (response) {
          if (!response.data.error) {
            toastr.success('Позиция успешно обновленна');
            $scope.post = response.data.data.item;
            console.log('post', $scope.post, response.data.data);
          } else {
            toastr.error(response.data.error.message);
          }
        }).catch(function (error) {
          toastr.error(response.data.error.errors.message);
        });
      }
    };

    $scope.delete = function (id) {
      if (confirm('Вы подтверждаете удаление?')) {
        RestService.delete('post/' + id).then(function (response) {

          if (response.data.status === 'deleted') {
            toastr.error('Продукция успешно удаленна');
            loadData();
          } else {
            toastr.info('Произошла ошибка удаления');
          }
        });
      }
    };

    $scope.uploader = new FileUploader({
      url: API_ENDPOINT.url + '/upload/post/' + $scope.id,
      headers: { Authorization: $http.defaults.headers.common.Authorization },
      removeAfterUpload: true,
      autoUpload: true
    });

    $scope.deleteImage = function (imageId) {
      if (confirm('Вы подтверждаете удаление картинки?')) {
        RestService.delete('gallery/' + imageId).then(function (response) {

          if (response.data.status === 'deleted') {
            toastr.success('Картинка успешно удаленна');

            RestService.get('gallery/post/' + $state.params.id, {}).then(function (response) {
              $scope.images = response.data.data;
            });
          } else {
            toastr.error('Произошла ошибка удаления');
          }
        });
      }
    };

    $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
      console.info('onSuccessItem', fileItem, response, status, headers);
      $scope.images.push({ dir: response.file.dir, image: response.file.image });
    };
    $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
      console.info('onErrorItem', fileItem, response, status, headers);
    };

    function loadData() {
      if ($state.params.id) {
        RestService.get('post/' + $state.params.id, {}).then(function (response) {
          $scope.post = response.data.data;

          RestService.get('post_category/', {}).then(function (responseCategory) {
            $scope.categories = responseCategory.data.data;
          });
        });

        RestService.get('gallery/post/' + $state.params.id, {}).then(function (response) {
          $scope.images = response.data.data;
        });
      }
    };
  };
})();

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {

  'use strict';

  angular.module('mainApp')

  /**
   * IndexController
   * Description: Sets up a controller
   */
  .controller('PostsController', PostsController);
  function PostsController($scope, $log, $http, RestService, API_PUBLIC, API_ENDPOINT, $state, toastr, FileUploader) {

    $scope.$watch(function () {
      return $state.params.cat;
    }, function (newVal, oldVal) {
      $log.log('$watch $state', newVal);
    });

    $scope.listProducts = [];
    $scope.cat = $state.params.cat;
    $scope.name = 'Посты';
    $scope.find = {};
    $scope.gridActions = {};
    $scope.publicUrl = API_PUBLIC.url;

    $scope.gridOptions = {
      data: [],
      sort: {},
      urlSync: true
    };

    loadData();

    $scope.uploader = new FileUploader({
      url: API_ENDPOINT.url + '/import/' + $scope.cat,
      headers: { Authorization: $http.defaults.headers.common.Authorization },
      autoUpload: true
    });

    $scope.uploader.onSuccessItem = function (res, responce) {
      console.log('Загрузил');
      if (responce.status === 'ok') {
        $('#before-load').fadeOut(200, function () {
          toastr.info('Файл успешно загружен');
          $scope.listProducts = responce.items;
        });
      } else {
        $('#before-load').fadeOut(200, function () {
          toastr.error(responce.error, 'Ошибка загрузки файла');
          $scope.listProducts = [];
        });
      }
    };

    $scope.uploader.onBeforeUploadItem = function () {
      $('#before-load').fadeIn(200);
    };

    $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
      console.info('onErrorItem', fileItem, response, status, headers);
      $('#before-load').fadeOut(200, function () {
        toastr.info('Произошла ошибка закачки Файла');
      });
    };

    $scope.delete = function (id) {
      if (confirm('Вы подтверждаете удаление?')) {
        RestService.delete('post/' + id).then(function (response) {

          if (response.data.status === 'deleted') {
            toastr.error('Продукция успешно удаленна');
            loadData();
          } else {
            toastr.info('Произошла ошибка удаления');
          }
        });
      }
    };

    function loadData() {
      RestService.get('post', {}).then(function (response) {
        $('table.table').show(400);
        $scope.listProducts = response.data.data;
        $scope.gridOptions.data = response.data.data;
        console.log('get products', response);
      });
    };
  };
})();

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {

  'use strict';

  angular.module('mainApp')

  /**
   * IndexController
   * Description: Sets up a controller
   */
  .controller('PostCategoryController', PostCategoryController);
  function PostCategoryController($scope, $log, $http, RestService, API_ENDPOINT, $state, toastr, FileUploader) {

    $scope.list = [];
    $scope.gridActions = {};
    $scope.gridOptions = {
      data: [],
      sort: {},
      urlSync: true
    };

    loadData();

    $scope.delete = function (id) {
      if (confirm('Вы подтверждаете удаление?')) {
        RestService.delete('post_category/' + id).then(function (response) {
          if (response.data.status === 'deleted') {
            toastr.error('Запись успешно удаленна');
            loadData();
          } else {
            toastr.info('Произошла ошибка удаления');
          }
        });
      }
    };

    function loadData() {
      RestService.get('post_category', {}).then(function (response) {
        $('table.table').show(400);
        $scope.list = response.data.data;
        $scope.gridOptions.data = response.data.data;
        console.log('get post category', response);
      });
    };
  };
})();

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {

    'use strict';

    angular.module('mainApp')

    /**
     * IndexController
     * Description: Sets up a controller
     */
    .controller('PostCategoryFormController', PostCategoryFormController);
    function PostCategoryFormController($scope, $log, $http, RestService, API_ENDPOINT, API_PUBLIC, $state, toastr, FileUploader) {

        $('#edit').froalaEditor({
            tabSpaces: 4
        });

        $scope.category = {};
        $scope.images = [];
        $scope.cat = $state.params.cat;
        $scope.id = $state.params.id;
        $scope.API_PUBLIC = API_PUBLIC;
        $scope.froalaOptions = {
            imageUploadURL: API_ENDPOINT.url + '/editor-upload'
        };
        loadData();

        $scope.saveCategory = function () {

            console.log('saveCategory', $scope.category);
            if (!$scope.category.id) {
                RestService.post('post_category/', { data: $scope.category }).then(function (response) {
                    if (!response.data.error) {
                        toastr.success('Позиция успешно добавленна');
                        $scope.category.id = response.data.data;
                    } else toastr.error(response.data.error);
                }, function (response) {
                    console.log('error-response', response);
                });
            } else {
                RestService.put('post_category/' + $scope.id, { data: $scope.category }).then(function (response) {
                    if (!response.data.error) toastr.success('Позиция успешно обновленна');else toastr.error(response.data.error);
                }, function (response) {
                    console.log('error-response', response);
                });
            }
        };

        $scope.delete = function (id) {
            if (confirm("Вы подтверждаете удаление?")) {
                RestService.delete("post_category/" + id).then(function (response) {

                    if (response.data.status === 'deleted') {
                        toastr.error('Продукция успешно удаленна');
                        loadData();
                    } else {
                        toastr.info('Произошла ошибка удаления');
                    }
                });
            }
        };

        $scope.uploader = new FileUploader({
            url: API_ENDPOINT.url + '/upload/post_category/' + $scope.id,
            headers: { Authorization: $http.defaults.headers.common.Authorization },
            removeAfterUpload: true,
            autoUpload: true
        });

        $scope.deleteImage = function (imageId) {
            if (confirm("Вы подтверждаете удаление картинки?")) {
                RestService.delete("gallery/" + imageId).then(function (response) {

                    if (response.data.status === 'deleted') {
                        toastr.success('Картинка успешно удаленна');

                        RestService.get("gallery/post_category/" + $state.params.id, {}).then(function (response) {
                            $scope.images = response.data.data;
                            console.log('gallery', response.data.data);
                        });
                    } else {
                        toastr.error('Произошла ошибка удаления');
                    }
                });
            }
        };

        $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
            $scope.images.push({ dir: response.file.dir, image: response.file.image });
        };
        $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };

        function loadData() {
            if ($state.params.id) {
                RestService.get("post_category/" + $state.params.id, {}).then(function (response) {
                    $scope.category = response.data.data;
                    console.log('get product', response);
                });

                RestService.get("gallery/post_category/" + $state.params.id, {}).then(function (response) {
                    $scope.images = response.data.data;
                    console.log('gallery', response.data.data);
                });
            }
        };
    };
})();

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = "<h1>Angular adapter for the Froala WYSIWYG editor</h1>\r\n<div class=\"sample\">\r\n    <h2>Sample 1: Inline Edit</h2>\r\n    <div froala=\"titleOptions\" ng-model=\"myTitle\"></div>\r\n</div>\r\n\r\n<div class=\"sample\">\r\n    <h2>Sample 2: Full Editor</h2>\r\n    <textarea id=\"froala-sample-2\" froala=\"titleOptions2\" ng-model=\"sample2Text\"></textarea>\r\n    <h4>Rendered Content:</h4>\r\n    <div froala-view=\"sample2Text\"></div>\r\n</div>\r\n\r\n<div class=\"sample\">\r\n    <h2>Sample 3: Manual Initialization</h2>\r\n    <button class=\"manual\" ng-click=\"initControls.initialize()\">Initialize Editor</button>\r\n    <button ng-click=\"initControls.destroy()\" ng-show=\"initControls.getEditor() != null\">Close Editor</button>\r\n    <button ng-click=\"deleteAll()\" ng-show=\"initControls.getEditor() != null\">Delete All</button>\r\n    <div id=\"froala-sample-3\" froala froala-init=\"initialize(initControls)\" ng-model=\"sample3Text\">Check out the <a href=\"https://www.froala.com/wysiwyg-editor\">Froala Editor</a></div>\r\n</div>\r\n\r\n<div class=\"sample\">\r\n    <h2>Sample 4: Editor on 'img' tag</h2>\r\n    <img froala ng-model=\"imgModel\"/>\r\n    <h4>Model Obj:</h4>\r\n    <div>{{imgModel}}</div>\r\n</div>\r\n\r\n<div class=\"sample\">\r\n    <h2>Sample 5: Editor on 'button' tag</h2>\r\n    <button froala ng-model=\"buttonModel\"></button>\r\n    <h4>Model Obj:</h4>\r\n    <div>{{buttonModel}}</div>\r\n</div>\r\n\r\n<div class=\"sample\">\r\n    <h2>Sample 6: Editor on 'input' tag</h2>\r\n    <input froala=\"inputOptions\" ng-model=\"inputModel\"/>\r\n    <h4>Model Obj:</h4>\r\n    <div>{{inputModel}}</div>\r\n</div>\r\n\r\n<div class=\"sample\">\r\n    <h2>Sample 7: Editor on 'a' tag. Manual Initialization</h2>\r\n    <button class=\"manual\" ng-click=\"linkInitControls.initialize()\">Initialize Editor</button>\r\n    <button ng-click=\"linkInitControls.destroy()\" ng-show=\"linkInitControls.getEditor() != null\">Close Editor</button>\r\n    <div>\r\n        <a froala froala-init=\"initializeLink(initControls)\" ng-model=\"linkModel\">Froala Editor</a>\r\n    </div>\r\n    <h4>Model Obj:</h4>\r\n    <div>{{linkModel}}</div>\r\n</div>";

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = "<div class=\"row tile_count animate bounceInDown \">\r\n    <div class=\"col-md-2 col-sm-4 col-xs-6 tile_stats_count\">\r\n        <span class=\"count_top\"><i class=\"fa fa-user\"></i> Категорий</span>\r\n        <div class=\"count\">0</div>\r\n        <span class=\"count_bottom\"><i class=\"green\"><i class=\"fa fa-sort-asc\"></i>0% </i> С последней недели</span>\r\n    </div>\r\n    <div class=\"col-md-2 col-sm-4 col-xs-6 tile_stats_count\">\r\n        <span class=\"count_top\"><i class=\"fa fa-user\"></i> Товаров</span>\r\n        <div class=\"count\">0</div>\r\n        <span class=\"count_bottom\"><i class=\"green\">0% </i> С последней недели</span>\r\n    </div>\r\n    <div class=\"col-md-2 col-sm-4 col-xs-6 tile_stats_count\">\r\n        <span class=\"count_top\"><i class=\"fa fa-clock-o\"></i> Просмотров</span>\r\n        <div class=\"count green\">0</div>\r\n        <span class=\"count_bottom\"><i class=\"green\"><i class=\"fa fa-sort-asc\"></i>0% </i> С последней недели</span>\r\n    </div>\r\n\r\n    <div class=\"col-md-2 col-sm-4 col-xs-6 tile_stats_count\">\r\n        <span class=\"count_top\"><i class=\"fa fa-user\"></i> Заказов</span>\r\n        <div class=\"count\">0</div>\r\n        <span class=\"count_bottom\"><i class=\"red\"><i class=\"fa fa-sort-desc\"></i>0% </i> С последней недели</span>\r\n    </div>\r\n    <div class=\"col-md-2 col-sm-4 col-xs-6 tile_stats_count\">\r\n        <span class=\"count_top\"><i class=\"fa fa-user\"></i> Обновлений</span>\r\n        <div class=\"count\">0</div>\r\n        <span class=\"count_bottom\"><i class=\"green\"><i class=\"fa fa-sort-asc\"></i>0% </i> С последней недели</span>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"col-md-12 col-sm-12 col-xs-12\">\r\n        <div class=\"dashboard_graph animate fadeInRight\">\r\n            <div class=\"row x_title\"><h3>Вы успешно авторизовались</h3></div>\r\n        </div>\r\n    </div>\r\n</div>";

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = "<div class=\"login_wrapper\">\r\n    <div class=\"animate form login_form wow bounceInLeft\">\r\n        <section class=\"login_content\">\r\n            <form>\r\n                <h1>Аторизация</h1>\r\n                <div>\r\n                    <input type=\"text\" class=\"form-control\"  ng-model=\"user.email\" placeholder=\"Email\" required=\"\" />\r\n                </div>\r\n                <div>\r\n                    <input type=\"password\" class=\"form-control\" ng-model=\"user.password\" placeholder=\"Password\" required=\"\" />\r\n                </div>\r\n                <div>\r\n                    <a class=\"btn btn-default submit\" href=\"#\" ng-click=\"login()\">Log in</a>\r\n                </div>\r\n\r\n                <div class=\"clearfix\"></div>\r\n            </form>\r\n        </section>\r\n    </div>\r\n</div>";

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = "<div class=\"menu_section\">\r\n    <h3>Основное</h3>\r\n    <ul class=\"nav side-menu\">\r\n        <li><a ui-sref=\"index\"><i class=\"fa fa-edit\"></i> Первая</a></li>\r\n        <li><a ui-sref=\"editor\"><i class=\"fa fa-edit\"></i> Редактор </a></li>\r\n        <li><a ui-sref=\"posts\"><i class=\"fa fa-edit\"></i> Посты </a></li>\r\n        <li><a ui-sref=\"post_category\"><i class=\"fa fa-edit\"></i> Категории</a></li>\r\n    </ul>\r\n</div>";

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-md-12 col-sm-12 col-xs-12\">\r\n        <div class=\"dashboard_graph form-add animate fadeInRight\">\r\n            <div class=\"row x_title\"><h3>Описание поста</h3></div>\r\n            <form class=\"animate fadeInDown product-form\">\r\n                <ng-include src=\"'app/views/post-form.html'\"></ng-include>\r\n                <div class=\"form-group animate bounceInLeft\">\r\n                    <button class=\"btn btn-info\" ui-sref=\"posts\">Назад</button>&nbsp;\r\n                    <button class=\"btn btn-success\" ng-click=\"saveProduct()\">Сохранить</button>\r\n                </div>\r\n            </form>\r\n            <br/>\r\n            <div class=\"row\">\r\n                <div class=\"col-xs-12\">\r\n                    <div class=\"x_title\">\r\n                        <h2><i class=\"fa fa-align-left\"></i> Gallery</h2>\r\n                        <div class=\"clearfix\"></div>\r\n                    </div>\r\n                    <div class=\"x_panel\">\r\n                        <div class=\"x_content\">\r\n                            <input type=\"file\" nv-file-select uploader=\"uploader\" multiple/><br/>\r\n                            <table class=\"table\" ng-show=\"uploader.queue.length >0 \">\r\n                                <thead>\r\n                                <tr>\r\n                                    <th width=\"50%\">Name</th>\r\n                                    <th ng-show=\"uploader.isHTML5\">Size</th>\r\n                                    <th ng-show=\"uploader.isHTML5\">Progress</th>\r\n                                    <th>Status</th>\r\n                                    <th>Actions</th>\r\n                                </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                <tr ng-repeat=\"item in uploader.queue\">\r\n                                    <td><strong>{{ item.file.name }}</strong></td>\r\n                                    <td ng-show=\"uploader.isHTML5\" nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td>\r\n                                    <td ng-show=\"uploader.isHTML5\">\r\n                                        <div class=\"progress\" style=\"margin-bottom: 0;\">\r\n                                            <div class=\"progress-bar\" role=\"progressbar\"\r\n                                                 ng-style=\"{ 'width': item.progress + '%' }\"></div>\r\n                                        </div>\r\n                                    </td>\r\n                                    <td class=\"text-center\">\r\n                                        <span ng-show=\"item.isSuccess\"><i class=\"glyphicon glyphicon-ok\"></i></span>\r\n                                        <span ng-show=\"item.isCancel\"><i class=\"glyphicon glyphicon-ban-circle\"></i></span>\r\n                                        <span ng-show=\"item.isError\"><i class=\"glyphicon glyphicon-remove\"></i></span>\r\n                                    </td>\r\n                                    <td nowrap>\r\n                                        <button type=\"button\" class=\"btn btn-success btn-xs\" ng-click=\"item.upload()\"\r\n                                                ng-disabled=\"item.isReady || item.isUploading || item.isSuccess\">\r\n                                            <span class=\"glyphicon glyphicon-upload\"></span> Upload\r\n                                        </button>\r\n                                        <button type=\"button\" class=\"btn btn-warning btn-xs\" ng-click=\"item.cancel()\"\r\n                                                ng-disabled=\"!item.isUploading\">\r\n                                            <span class=\"glyphicon glyphicon-ban-circle\"></span> Cancel\r\n                                        </button>\r\n                                        <button type=\"button\" class=\"btn btn-danger btn-xs\" ng-click=\"item.remove()\">\r\n                                            <span class=\"glyphicon glyphicon-trash\"></span> Remove\r\n                                        </button>\r\n                                    </td>\r\n                                </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-xs-12\" ng-if=\"images && images.length >0\">\r\n                    <div class=\"x_panel\">\r\n                        <div class=\"x_content\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-55\" ng-repeat=\"item in images\">\r\n                                    <div class=\"thumbnail thumbnailMy\">\r\n                                        <div class=\"image view view-first\">\r\n                                            <img src=\"{{API_PUBLIC.url + item.dir+'big_'+item.image}}\" alt=\"image\" />\r\n                                            <div class=\"mask\">\r\n                                                <p></p>\r\n                                                <div class=\"tools tools-bottom\">\r\n                                                    <a target=\"_blank\" href=\"{{item.dir + item.image}}\"><i class=\"fa fa-eye\"></i></a>\r\n                                                    <a ng-click=\"deleteImage(item._id)\"><i class=\"fa fa-times\"></i></a>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>";

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = "<div class=\"panel-group\">\r\n    <!-- 1 панель -->\r\n    <div class=\"panel panel-default\">\r\n        <div class=\"panel-heading\">\r\n            <h4 class=\"panel-title cursor-pointer\" ng-click=\"toggle('#according01')\">Основные поля</h4>\r\n        </div>\r\n        <div class=\"panel-body\" id=\"according01\">\r\n            <div class=\"form-group\">\r\n                <label for=\"name\">Название:</label>\r\n                <input type=\"text\" class=\"form-control\" required id=\"name\" ng-model=\"post.name\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"name\">Slug:</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"slug\" ng-model=\"post.slug\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"name\">Дата:</label>\r\n                <quick-datepicker ng-model='post.date' icon-class='fa fa-calendar' label-format='dd.MM.yyyy' disable-timepicker=\"true\"></quick-datepicker>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"name\">Категория:</label>\r\n                <select class=\"form-control\" required id=\"category\" ng-model=\"post.category.id\">\r\n                    <option ng-repeat=\"category in categories\" value=\"{{category.id}}\">{{category.name}}</option>\r\n                </select>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"title\">Заголовок:</label>\r\n                <input type=\"text\" class=\"form-control\" required id=\"title\" ng-model=\"post.title\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"small\">Краткое описание:</label>\r\n                <textarea class=\"form-control\" id=\"small\" ng-model=\"post.small\"></textarea>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"description\">Описание:</label>\r\n                <textarea froala=\"froalaOptions\" class=\"form-control\" id=\"description\" ng-model=\"post.description\"></textarea>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"panel-group\">\r\n    <!-- 1 панель -->\r\n    <div class=\"panel panel-default\">\r\n        <div class=\"panel-heading\">\r\n            <h4 class=\"panel-title cursor-pointer\" ng-click=\"toggle('#according02')\">Мета</h4>\r\n        </div>\r\n        <div class=\"panel-body display-none\" id=\"according02\">\r\n            <div class=\"form-group\">\r\n                <label for=\"metatitle\">Meta Title:</label>\r\n                <textarea class=\"form-control\" id=\"metatitle\" ng-model=\"post.metatitle\"></textarea>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"metadescription\">Meta Description:</label>\r\n                <textarea class=\"form-control\" id=\"metadescription\" ng-model=\"post.metadescription\"></textarea>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"metakeys\">Meta Keys:</label>\r\n                <textarea class=\"form-control\" id=\"metakeys\" ng-model=\"post.metakeys\"></textarea>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<input type=\"hidden\" id=\"id\" ng-model=\"post.id\">";

/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-md-12 col-sm-12 col-xs-12\">\r\n        <div class=\"dashboard_graph form-add animate fadeInRight\">\r\n            <div class=\"row x_title\"><h3>Посты</h3></div>\r\n            <div class=\"form-group animate bounceInLeft text-right\">\r\n                <button ui-sref=\"post\" class=\"btn btn-success btn-sm\"><i class=\"fa fa-plus\"></i> Добавить\r\n                </button>\r\n            </div>\r\n            <br/>\r\n\r\n            <div grid-data id='grid1' grid-options=\"gridOptions\" grid-actions=\"gridActions\">\r\n                <form class=\"form-inline pull-right margin-bottom-basic\">\r\n                    <div class=\"form-group\">\r\n                        <grid-pagination max-size=\"10\"\r\n                                         boundary-links=\"true\"\r\n                                         class=\"pagination-sm\"\r\n                                         total-items=\"paginationOptions.totalItems\"\r\n                                         ng-model=\"paginationOptions.currentPage\"\r\n                                         ng-change=\"reloadGrid()\"\r\n                                         items-per-page=\"paginationOptions.itemsPerPage\"></grid-pagination>\r\n                    </div>\r\n                    <div class=\"form-group items-per-page\">\r\n                        <label for=\"itemsOnPageSelect2\">Items per page:</label>\r\n                        <select id=\"itemsOnPageSelect2\" class=\"form-control input-sm\"\r\n                                ng-init=\"paginationOptions.itemsPerPage = '10'\"\r\n                                ng-model=\"paginationOptions.itemsPerPage\" ng-change=\"reloadGrid()\">\r\n                            <option>10</option>\r\n                            <option>25</option>\r\n                            <option>50</option>\r\n                            <option>75</option>\r\n                        </select>\r\n                    </div>\r\n                </form>\r\n                <table class=\"table table-bordered table-striped\">\r\n                    <thead>\r\n                    <tr>\r\n                        <th>Фото</th>\r\n                        <th class=\"st-sort-disable\">Название</th>\r\n                        <th class=\"st-sort-disable\">Категория</th>\r\n                        <th></th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                    <tr grid-item>\r\n                        <td><img ng-if=\"item.image.file\" src=\"{{publicUrl+item.image.dir+'thumb_'+item.image.file}}\" /></td>\r\n                        <td width=\"30%\" ng-bind=\"item.name\"></td>\r\n                        <td width=\"30%\" ng-bind=\"item.category.name\"></td>\r\n                        <td>\r\n                            <button ui-sref=\"post({id:item.id})\" class=\"btn btn-primary\"><i\r\n                                    class=\"fa fa-pencil\"></i></button>\r\n                            <button class=\"btn btn-danger\" ng-click=\"delete(item.id)\"><i class=\"fa fa-times\"></i>\r\n                            </button>\r\n                        </td>\r\n                    </tr>\r\n                    </tbody>\r\n                </table>\r\n\r\n                <form class=\"form-inline pull-right margin-bottom-basic\">\r\n                    <div class=\"form-group\">\r\n                        <grid-pagination max-size=\"15\"\r\n                                         boundary-links=\"true\"\r\n                                         class=\"pagination-sm\"\r\n                                         total-items=\"paginationOptions.totalItems\"\r\n                                         ng-model=\"paginationOptions.currentPage\"\r\n                                         ng-change=\"reloadGrid()\"\r\n                                         items-per-page=\"paginationOptions.itemsPerPage\"></grid-pagination>\r\n                    </div>\r\n                    <div class=\"form-group items-per-page\">\r\n                        <label for=\"itemsOnPageSelect2\">Items per page:</label>\r\n                        <select id=\"itemsOnPageSelect2\" class=\"form-control input-sm\"\r\n                                ng-init=\"paginationOptions.itemsPerPage = '10'\"\r\n                                ng-model=\"paginationOptions.itemsPerPage\" ng-change=\"reloadGrid()\">\r\n                            <option>10</option>\r\n                            <option>25</option>\r\n                            <option>50</option>\r\n                            <option>75</option>\r\n                        </select>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n            <div class=\"form-group animate bounceInLeft text-right col-xs-12\">\r\n                <button ui-sref=\"post\" class=\"btn btn-success btn-sm\"><i class=\"fa fa-plus\"></i>\r\n                    Добавить\r\n                </button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>";

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-md-12 col-sm-12 col-xs-12\">\r\n        <div class=\"dashboard_graph form-add animate fadeInRight\">\r\n            <div class=\"row x_title\"><h3>Категории постов</h3></div>\r\n            <div class=\"form-group animate bounceInLeft text-right\">\r\n                <button ui-sref=\"post_category_item\" class=\"btn btn-success btn-sm\"><i class=\"fa fa-plus\"></i> Добавить\r\n                </button>\r\n            </div>\r\n            <br/>\r\n\r\n            <div grid-data id='grid1' grid-options=\"gridOptions\" grid-actions=\"gridActions\">\r\n                <table class=\"table table-bordered table-striped\">\r\n                    <thead>\r\n                    <tr>\r\n                        <th class=\"st-sort-disable\">\r\n                            Название\r\n                        </th>\r\n                        <th></th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                    <tr grid-item>\r\n                        <td width=\"30%\" ng-bind=\"item.name\"></td>\r\n                        <td>\r\n                            <button ui-sref=\"post_category_item({id:item.id})\" class=\"btn btn-primary\"><i\r\n                                    class=\"fa fa-pencil\"></i></button>\r\n                            &nbsp;\r\n                            <button class=\"btn btn-danger\" ng-click=\"delete(item.id)\"><i class=\"fa fa-times\"></i>\r\n                            </button>\r\n                        </td>\r\n                    </tr>\r\n                    </tbody>\r\n                </table>\r\n            </div>\r\n            <div class=\"form-group animate bounceInLeft text-right col-xs-12\">\r\n                <button ui-sref=\"post_category_item\" class=\"btn btn-success btn-sm\"><i class=\"fa fa-plus\"></i>\r\n                    Добавить\r\n                </button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>";

/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-md-12 col-sm-12 col-xs-12\">\r\n        <div class=\"dashboard_graph form-add animate fadeInRight\">\r\n            <div class=\"row x_title\"><h3>Описание категории</h3></div>\r\n            <form class=\"animate fadeInDown product-form\">\r\n                <ng-include src=\"'app/views/post-category-form.html'\"></ng-include>\r\n                <div class=\"form-group animate bounceInLeft\">\r\n                    <button class=\"btn btn-info\" ui-sref=\"post_category\">Назад</button>&nbsp;\r\n                    <button class=\"btn btn-success\" ng-click=\"saveCategory()\">Сохранить</button>\r\n                </div>\r\n            </form>\r\n            <br/>\r\n            <div class=\"row\">\r\n                <div class=\"col-xs-12\">\r\n                    <div class=\"x_title\">\r\n                        <h2><i class=\"fa fa-align-left\"></i> Gallery</h2>\r\n                        <div class=\"clearfix\"></div>\r\n                    </div>\r\n                    <div class=\"x_panel\">\r\n                        <div class=\"x_content\">\r\n                            <input type=\"file\" nv-file-select uploader=\"uploader\" multiple/><br/>\r\n                            <table class=\"table\" ng-show=\"uploader.queue.length >0 \">\r\n                                <thead>\r\n                                <tr>\r\n                                    <th width=\"50%\">Name</th>\r\n                                    <th ng-show=\"uploader.isHTML5\">Size</th>\r\n                                    <th ng-show=\"uploader.isHTML5\">Progress</th>\r\n                                    <th>Status</th>\r\n                                    <th>Actions</th>\r\n                                </tr>\r\n                                </thead>\r\n                                <tbody>\r\n                                <tr ng-repeat=\"item in uploader.queue\">\r\n                                    <td><strong>{{ item.file.name }}</strong></td>\r\n                                    <td ng-show=\"uploader.isHTML5\" nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td>\r\n                                    <td ng-show=\"uploader.isHTML5\">\r\n                                        <div class=\"progress\" style=\"margin-bottom: 0;\">\r\n                                            <div class=\"progress-bar\" role=\"progressbar\"\r\n                                                 ng-style=\"{ 'width': item.progress + '%' }\"></div>\r\n                                        </div>\r\n                                    </td>\r\n                                    <td class=\"text-center\">\r\n                                        <span ng-show=\"item.isSuccess\"><i class=\"glyphicon glyphicon-ok\"></i></span>\r\n                                        <span ng-show=\"item.isCancel\"><i class=\"glyphicon glyphicon-ban-circle\"></i></span>\r\n                                        <span ng-show=\"item.isError\"><i class=\"glyphicon glyphicon-remove\"></i></span>\r\n                                    </td>\r\n                                    <td nowrap>\r\n                                        <button type=\"button\" class=\"btn btn-success btn-xs\" ng-click=\"item.upload()\"\r\n                                                ng-disabled=\"item.isReady || item.isUploading || item.isSuccess\">\r\n                                            <span class=\"glyphicon glyphicon-upload\"></span> Upload\r\n                                        </button>\r\n                                        <button type=\"button\" class=\"btn btn-warning btn-xs\" ng-click=\"item.cancel()\"\r\n                                                ng-disabled=\"!item.isUploading\">\r\n                                            <span class=\"glyphicon glyphicon-ban-circle\"></span> Cancel\r\n                                        </button>\r\n                                        <button type=\"button\" class=\"btn btn-danger btn-xs\" ng-click=\"item.remove()\">\r\n                                            <span class=\"glyphicon glyphicon-trash\"></span> Remove\r\n                                        </button>\r\n                                    </td>\r\n                                </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-xs-12\" ng-if=\"images && images.length >0\">\r\n                    <div class=\"x_panel\">\r\n                        <div class=\"x_content\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-md-55\" ng-repeat=\"item in images\">\r\n                                    <div class=\"thumbnail thumbnailMy\">\r\n                                        <div class=\"image view view-first\">\r\n                                            <img src=\"{{API_PUBLIC.url + item.dir+'big_'+item.image}}\" alt=\"image\" />\r\n                                            <div class=\"mask\">\r\n                                                <p></p>\r\n                                                <div class=\"tools tools-bottom\">\r\n                                                    <a target=\"_blank\" href=\"{{item.dir + item.image}}\"><i class=\"fa fa-eye\"></i></a>\r\n                                                    <a ng-click=\"deleteImage(item._id)\"><i class=\"fa fa-times\"></i></a>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>";

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = "<div class=\"panel-group\">\r\n    <!-- 1 панель -->\r\n    <div class=\"panel panel-default\">\r\n        <div class=\"panel-heading\">\r\n            <h4 class=\"panel-title cursor-pointer\" ng-click=\"toggle('#according01')\">Основные поля</h4>\r\n        </div>\r\n        <div class=\"panel-body\" id=\"according01\">\r\n            <div class=\"form-group\">\r\n                <label for=\"name\">Название:</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"name\" ng-model=\"category.name\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"title\">Заголовок:</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"title\" ng-model=\"category.title\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"small\">Краткое описание:</label>\r\n                <textarea class=\"form-control\" id=\"small\" ng-model=\"category.small\"></textarea>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"description\">Описание:</label>\r\n                <textarea froala=\"froalaOptions\" class=\"form-control\" id=\"description\" ng-model=\"category.description\"></textarea>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"panel-group\">\r\n    <!-- 1 панель -->\r\n    <div class=\"panel panel-default\">\r\n        <div class=\"panel-heading\">\r\n            <h4 class=\"panel-title cursor-pointer\" ng-click=\"toggle('#according02')\">Мета</h4>\r\n        </div>\r\n        <div class=\"panel-body display-none\" id=\"according02\">\r\n            <div class=\"form-group\">\r\n                <label for=\"metatitle\">Meta Title:</label>\r\n                <textarea class=\"form-control\" id=\"metatitle\" ng-model=\"category.metatitle\"></textarea>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"metadescription\">Meta Description:</label>\r\n                <textarea class=\"form-control\" id=\"metadescription\" ng-model=\"category.metadescription\"></textarea>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label for=\"metakeys\">Meta Keys:</label>\r\n                <textarea class=\"form-control\" id=\"metakeys\" ng-model=\"category.metakeys\"></textarea>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<input type=\"hidden\" id=\"id\" ng-model=\"category.id\">";

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


angular.module('mainApp').service('AuthService', function ($q, $http, API_ENDPOINT) {
    var LOCAL_TOKEN_KEY = 'token';
    var authenticated = false;
    var authToken = void 0;

    function loadUserCredentials() {
        var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
        if (token) {
            useCredentials(token);
            console.log("loadUserCredentials");
        }
    }

    function storeUserCredentials(token) {
        window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
        useCredentials(token);
    }

    function useCredentials(token) {
        authenticated = true;
        authToken = token;
        console.log('authenticated', authenticated);

        // Set the token as header for your requests!
        $http.defaults.headers.common.Authorization = 'JWT ' + authToken;
    }

    function destroyUserCredentials() {
        authToken = undefined;
        authenticated = false;
        $http.defaults.headers.common.Authorization = undefined;
        window.localStorage.removeItem(LOCAL_TOKEN_KEY);
    }

    var register = function register(user) {
        return $q(function (resolve, reject) {
            $http.post(API_ENDPOINT.url + '/signup', user).then(function (result) {
                if (result.data.success) {
                    resolve(result.data.msg);
                } else {
                    reject(result.data.msg);
                }
            });
        });
    };

    var login = function login(user) {
        return $q(function (resolve, reject) {
            $http.post(API_ENDPOINT.url + '/auth', { email: user.email, password: user.password }).then(function (result) {
                if (result.data.success) {

                    storeUserCredentials(result.data.token);
                    resolve(result.data.msg);
                } else {
                    reject(result.data.msg);
                }
            });
        });
    };

    var logout = function logout() {
        destroyUserCredentials();
    };

    loadUserCredentials();

    return {
        login: login,
        register: register,
        logout: logout,
        isAuthenticated: function isAuthenticated() {
            return authenticated;
        }
    };
}).factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
    return {
        responseError: function responseError(response) {
            $rootScope.$broadcast({
                401: AUTH_EVENTS.notAuthenticated
            }[response.status], response);
            return $q.reject(response);
        }
    };
}).config(function ($httpProvider) {
    console.log('httpProvider', $httpProvider.interceptors);
    $httpProvider.interceptors.push('AuthInterceptor');
});

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


angular.module('mainApp').service('RestService', function ($q, $http, API_ENDPOINT) {

    function send(method, url, data) {

        return $http({
            method: method,
            url: API_ENDPOINT.url + '/' + url,
            data: data
        });
    }

    function get(url, data) {
        return send('GET', url, data);
    }

    function put(url, data) {
        return send('PUT', url, data);
    }

    function post(url, data) {
        return send('POST', url, data);
    }

    function del(url, data) {
        return send('DELETE', url, data);
    }

    return {
        post: post,
        put: put,
        get: get,
        delete: del
    };
});

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


angular.module('mainApp').constant('AUTH_EVENTS', {
  notAuthenticated: 'auth-not-authenticated'
}).constant('API_PUBLIC', {
  url: 'http://127.0.0.1:8030/'
}).constant('API_ENDPOINT', {
  url: 'http://127.0.0.1:8030/api' //188.120.241.78
});

/***/ })
/******/ ]);