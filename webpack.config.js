module.exports = {
  entry: './app/app.js',
  output: {
    filename: './app.bundle.js',
    library: 'mainApp'
  },
  module: {
    rules: [
      {test: /\.html/, use: ['html-loader']},
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      }
    ]
  },
  watch: true
};